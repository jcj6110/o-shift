# Electrical Schematics README.md

Currently the structure of this branch is as follows:
The top level folder containts

1. README.md
2. .gitignore
3. Schematics Directory

In the schematics directory there will be:

1. **o-shift-schematic-symbols.lib**  -  This file will contain all the added symbols used in all schematics and should be referenced by all schematics.  This will make it easier to merge schematics and keep track of new symbols created.
2. subdirectory for each subcircuit under creation.  These will be merged as we get into specifying individual boards.

Each subcircuit is its own KiCAD project and may contain schematic files, layout files, -cache.lib files, dcm files, sym-lib-tables, but **no main .lib files** since they are contained in **o-shift-schematic-symbols.lib**.

There will also be subdirectories in each subcircuit for a BOM, pdfs of Datasheets, etc.

And example would be 

### MAIN DIRECTORY

-> README.md

-> .gitignore

-> Schematics/

	-> o-shift-schematic-symbols.lib

	-> solenoid-drive/

	-> display/

		-> display.pro

		-> display.sch

		-> sym-lib-table

		-> display-cache.lib

		-> display.kicad_pcb

		-> Datasheets/

		-> BOM/

