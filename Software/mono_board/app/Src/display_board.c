/*
 * File: display_board.c
 * Author: John Jenco <jcj6110@rit.edu>
 *
 * Main application code for display board.
 */

#include "display_board.h"
#include "shifter_dev.h"
#include "gpio.h"
#include "can.h"
#include "quadspi.h"
#include "EVE.h"
#include "EVE_config.h"
#include "EVE_commands.h"
#include "tft_data.h"
#include "tft.h"
#include <string.h>
#include <stdio.h>

#define DEBUG (1)

#define PID_REQUEST_ID       (0x7DF)
#define PID_RESPONSE_ID      (0x7E8)
#define PID_BYTE_0           (0x02)
#define PID_BYTE_1           (0x01)
#define PID_VEHICLE_SPEED    (0x0D)


static gear_t currentGear = NEUTRAL;
static gear_t idealGear = NEUTRAL;
static clutch_state_t currentClutchState = DISENGAGED;
static uint8_t vehicleSpeed = 0;
static uint16_t rpm1, rpm2, rpm3, rpm4, rpm5, rpm6 = 0;
static uint16_t simulatedRPM = 0;


void app_init()
{
  HAL_Init();
  SystemClock_Config();
  GPIO_Init();
  uart_init();
  MX_QUADSPI_Init();
  can_init();
  EVE_init();
  for (int i = 0; i < 150000; i++);
  TFT_init();
}

void app_run()
{
    TFT_loop(0, 'N', 'N', "Hello");
    while(1)
    {
        // Poll shifter board for status
        query_shifter();

        // Grab speed data from OBD2
        query_obd2();

        // Calculate and display ideal gear based on speed
        calculate_ideal_gear();

        // Update display with new values
        update_display();

        #if DEBUG
        print_info();
        #endif

        for(uint32_t i = 0; i < 1000; i++);

    }
}

void query_shifter()
{
    Can_Msg query;
    Can_Msg response;

    // Query for shifter status
    query.id = 0x01;
    query.dlc = 1;
    query.payload[0] = 0x55;

    can1_transmit(&query);

    if (can1_receive(&response) == 1)
    {
        currentGear = (gear_t)response.payload[0];
        currentClutchState = (clutch_state_t)response.payload[1];
        led_int_mask(0x01);
        for(uint32_t i = 0; i < 100000; i++);
        for(uint32_t i = 0; i < 100000; i++);

    }
}


void query_obd2()
{
    Can_Msg query;
    Can_Msg response;

    query.id  = PID_REQUEST_ID;
    query.dlc = 8;
    query.payload[0] = PID_BYTE_0;
    query.payload[1] = PID_BYTE_1;
    query.payload[2] = PID_VEHICLE_SPEED;
    query.payload[3] = 0x00;
    query.payload[4] = 0x00;
    query.payload[5] = 0x00;
    query.payload[6] = 0x00;
    query.payload[7] = 0x00;

    can2_transmit(&query);

    if (can2_receive(&response) == 1)
    {
        led_int_mask(0x2);
        if ((PID_RESPONSE_ID == response.id) && (PID_VEHICLE_SPEED == response.payload[2]))
        {
            led_int_mask(0x4);
            vehicleSpeed = response.payload[3];
        }
    }
}


void update_display()
{
    char currentGearChar, idealGearChar;
    // char gearChar;
    char buff[15] = {0};

    switch(currentGear)
    {
        case REV:
            currentGearChar = 'R';
            simulatedRPM = rpm1;
            break;
        case NEUTRAL:
            currentGearChar = 'N';
            simulatedRPM = IDLE_RPM;
            break;
        case FIRST:
            currentGearChar = '1';
            simulatedRPM = rpm1;
            break;
        case SECOND:
            currentGearChar = '2';
            simulatedRPM = rpm2;
            break;
        case THIRD:
            currentGearChar = '3';
            simulatedRPM = rpm3;
            break;
        case FOURTH:
            currentGearChar = '4';
            simulatedRPM = rpm4;
            break;
        case FIFTH:
            currentGearChar = '5';
            simulatedRPM = rpm5;
            break;
        case SIXTH:
            currentGearChar = '6';
            simulatedRPM = rpm6;
            break;
        default:
            currentGearChar = 'N';
            simulatedRPM = IDLE_RPM;
            break;
    }

    switch(idealGear)
    {
        case REV:     idealGearChar = 'R'; break;
        case NEUTRAL: idealGearChar = 'N'; break;
        case FIRST:   idealGearChar = '1'; break;
        case SECOND:  idealGearChar = '2'; break;
        case THIRD:   idealGearChar = '3'; break;
        case FOURTH:  idealGearChar = '4'; break;
        case FIFTH:   idealGearChar = '5'; break;
        case SIXTH:   idealGearChar = '6'; break;
        default:      idealGearChar = 'N'; break;
    }

    if (simulatedRPM == 0)
    {
        sprintf(buff, "WARNING-STALL");
    }
    else if (simulatedRPM == REDLINE_RPM)
    {
        sprintf(buff, "WARNING-REDLINE");
    }
    else
    {
        sprintf(buff, "Shifted to %c", currentGearChar);
    }

    TFT_loop(simulatedRPM, currentGearChar, idealGearChar, buff);
}

void calculate_ideal_gear()
{
    // Find the RPM for each gear with vehicle speed as the index.
    // If the speed is not valid, RPM is set to 0.
    // TODO: Need some other way to guard against index overrun?
    rpm1 = (vehicleSpeed < FIRST_GEAR_LIMIT)  ? rpm_table1[vehicleSpeed] : REDLINE_RPM;
    rpm2 = (vehicleSpeed < SECOND_GEAR_LIMIT) ? rpm_table2[vehicleSpeed] : REDLINE_RPM;
    rpm3 = (vehicleSpeed < THIRD_GEAR_LIMIT)  ? rpm_table3[vehicleSpeed] : REDLINE_RPM;
    rpm4 = (vehicleSpeed < FOURTH_GEAR_LIMIT) ? rpm_table4[vehicleSpeed] : REDLINE_RPM;
    rpm5 = (vehicleSpeed < FIFTH_GEAR_LIMIT)  ? rpm_table5[vehicleSpeed] : REDLINE_RPM;

    // Note: Fake 6th gear since no valid data was collected for 6th gear
    rpm6 = ((vehicleSpeed < SIXTH_GEAR_LIMIT) && (vehicleSpeed >= 10)) ? rpm_table5[vehicleSpeed-10] : REDLINE_RPM;

    // If we pick reverse, ideal is always reverse
    if (currentGear == REV)
    {
        idealGear = REV;
    }
    else
    {
        // Vehicle speed < 10kph, assume 1st gear
        if (vehicleSpeed < 10)
        {
            idealGear = FIRST; // TODO: Does this need to be here?
        }
        else
        {
            // Working our way down from 6th gear
            // Find the first gear that is valid and above a min RPM threshold
            if (rpm6 < MIN_SIXTH_GEAR_CRUISE_RPM)
            {
                if (rpm5 < MIN_FIFTH_GEAR_CRUISE_RPM)
                {
                    if (rpm4 < MIN_FOURTH_GEAR_CRUISE_RPM)
                    {
                        if (rpm3 < MIN_THIRD_GEAR_CRUISE_RPM)
                        {
                            if (rpm2 < MIN_SECOND_GEAR_CRUISE_RPM)
                            {
                                idealGear = FIRST;
                            }
                            else
                            {
                                idealGear = SECOND;
                            }
                        }
                        else
                        {
                            idealGear = THIRD;
                        }
                    }
                    else
                    {
                        idealGear = FOURTH;
                    }
                }
                else
                {
                    idealGear = FIFTH;
                }
            }
            else
            {
                idealGear = SIXTH;
            }
        }
    }
}

void print_info()
{
    char dbgMsg[60] = {0x00};

    sprintf(dbgMsg, "Speed: %03d Sim RPM: %04d Current: %01d Ideal: %01d Clutch: %01d\r\n", vehicleSpeed,
                                                                                            simulatedRPM,
                                                                                    (uint8_t)currentGear,
                                                                                    (uint8_t)idealGear,
                                                                                    (uint8_t)currentClutchState);
    uart_puts(dbgMsg, 60, 10);
}