/**
  ******************************************************************************
  * File Name          : gpio.c
  * Description        : This file provides code for the configuration
  *                      of all used GPIO pins.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

#include "shifter.h"

static gear_t curr_gear = NEUTRAL;
static gear_t prev_gear = NEUTRAL;
static state_t state = RESET_STATE;
static clutch_state_t clutch = DISENGAGED; 

void app_init(void) {
  HAL_Init();
  SystemClock_Config();
  GPIO_Init();
  uart_init();
  can_init();
  lockout_en();
  return;
}

void app_run(void) {
  while(1) {
    if (comms_get_request()) {
      comms_send_packet(curr_gear, clutch);
    }
    switch(state) {
      case RESET_STATE: r_state(); break;
      case NEUTRAL_STATE: n_state(); break;
      case TRANSITION_STATE: t_state(); break;
      case IN_GEAR_STATE: g_state(); break;
      default: break;
    }
  }
}

void r_state() {
  lockout_en();
  curr_gear = prev_gear;
  clutch = DISENGAGED;
  if (check_clutch()) {
    state = NEUTRAL_STATE;
  }
  //led_int_mask(curr_gear);
  return;
}

void n_state() {
  lockout_dis();
  curr_gear = NEUTRAL;
  clutch = ENGAGED;
  if (check_gear() > 0) {
    state = TRANSITION_STATE;
  }
  if (!check_clutch()) {
    state = RESET_STATE;
  }
  //led_int_mask(curr_gear);
  return;
}

void t_state() {
  lockout_dis();
  curr_gear = check_gear();
  clutch = ENGAGED;
  if (!check_clutch()) {
    state = IN_GEAR_STATE;
  }
  //led_int_mask(curr_gear);
  return;
}

void g_state() {
  lockout_en();
  clutch = DISENGAGED;
  if (check_clutch()) {
    prev_gear = curr_gear;
    state = NEUTRAL_STATE;
  }
  led_int_mask(curr_gear);
  return;
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/