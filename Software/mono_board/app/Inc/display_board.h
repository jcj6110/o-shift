/*
 * File: display_board.h
 * Author: John Jenco <jcj6110@rit.edu>
 *
 * Main application code for display board.
 */

#include <stdint.h>

void app_init(void);
void app_run(void);
void query_shifter(void);
void update_display(void);
void query_obd2(void);
void calculate_ideal_gear(void);
void print_info(void);

#define FIRST_GEAR_LIMIT (28)
#define SECOND_GEAR_LIMIT (70)
#define THIRD_GEAR_LIMIT (100)
#define FOURTH_GEAR_LIMIT (115)
#define FIFTH_GEAR_LIMIT (127)
#define SIXTH_GEAR_LIMIT (137)

#define SIXTH_GEAR_THRESHOLD (3000)

// Totally arbitrary, but I'd assume if you're cruising you keep the RPM lower in higher gear.
#define MIN_SECOND_GEAR_CRUISE_RPM (1400)
#define MIN_THIRD_GEAR_CRUISE_RPM (1300)
#define MIN_FOURTH_GEAR_CRUISE_RPM (1500)
#define MIN_FIFTH_GEAR_CRUISE_RPM (1900)
#define MIN_SIXTH_GEAR_CRUISE_RPM (1900)

#define IDLE_RPM (1000)
#define REDLINE_RPM (6000)

// Speed-indexed RPM mappings for 1st gear
static uint16_t rpm_table1[FIRST_GEAR_LIMIT] =
{
915,
982,
1049,
1116,
1182,
1249,
1316,
1383,
1450,
1516,
1583,
1650,
1717,
1783,
1850,
1917,
2084,
2251,
2418,
2752,
2919,
3086,
3286,
3486,
3687,
3887,
4087,
4288, // ~28 KPH
};

// Speed-indexed RPM mappings for 2nd gear
static uint16_t rpm_table2[SECOND_GEAR_LIMIT] =
{
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
1304, // 17 KPH
1389,
1474,
1560,
1645,
1731,
1816,
1902,
1987,
2059,
2131,
2203,
2275,
2419,
2491,
2563,
2635,
2707,
2779,
2851,
2923,
2994,
3062,
3129,
3196,
3263,
3331,
3398,
3465,
3532,
3600,
3667,
3734,
3801,
3869,
3936,
4013,
4091,
4246,
4323,
4401,
4479,
4556,
4634,
4711,
4789,
4866,
4944,
5021,
5176,
5254,
5331, // ~70 KPH
};

// Speed-indexed RPM mappings for 3rd gear
static uint16_t rpm_table3[THIRD_GEAR_LIMIT] =
{
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
1279, // ~29 KPH
1352,
1424,
1496,
1569,
1641,
1713,
1785,
2043,
2083,
2123,
2163,
2203,
2243,
2284,
2324,
2364,
2404,
2444,
2484,
2525,
2565,
2605,
2645,
2685,
2725,
2766,
2806,
2846,
2886,
2926,
2966,
3014,
3061,
3108,
3156,
3203,
3250,
3298,
3345,
3393,
3440,
3487,
3535,
3582,
3629,
3677,
3724,
3771,
3819,
3866,
3913,
3961,
4005,
4048,
4092,
4136,
4180,
4223,
4267,
4311,
4355,
4399,
4442,
4486,
4530,
4574,
4617,
4661, // ~100 KPH
};

// Speed-indexed RPM mappings for 4th gear
static uint16_t rpm_table4[FOURTH_GEAR_LIMIT] =
{
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
1428, // ~42 KPH
1461,
1495,
1528,
1562,
1595,
1629,
1662,
1696,
1729,
1762,
1796,
1829,
1863,
1896,
1930,
1963,
1997,
2030,
2064,
2097,
2130,
2164,
2197,
2231,
2264,
2298,
2331,
2365,
2398,
2431,
2465,
2498,
2532,
2565,
2599,
2632,
2666,
2699,
2732,
2766,
2799,
2833,
2866,
2900,
2933,
2967,
3000,
3033,
3067,
3100,
3134,
3167,
3201,
3234,
3268,
3301,
3335,
3368,
3401,
3435,
3468,
3502,
3535,
3569,
3602,
3636,
3669,
3702,
3736,
3769,
3803,
3836, // ~115 KPH
};

// Speed-indexed RPM mappings for 5th gear
static uint16_t rpm_table5[FIFTH_GEAR_LIMIT] =
{
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
1874, // ~67 KPH
1900,
1925,
1951,
1976,
2002,
2027,
2053,
2078,
2104,
2129,
2155,
2180,
2206,
2231,
2257,
2282,
2307,
2333,
2358,
2384,
2409,
2435,
2460,
2486,
2511,
2537,
2562,
2588,
2613,
2639,
2664,
2690,
2715,
2741,
2766,
2792,
2817,
2843,
2868,
2894,
2919,
2945,
2970,
2996,
3021,
3047,
3072,
3098,
3123,
3149,
3174,
3199,
3225,
3250,
3276,
3301,
3327,
3352,
3378, // ~127 KPH
};

