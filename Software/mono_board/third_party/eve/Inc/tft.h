
#ifndef TFT_H_
#define TFT_H_

extern uint16_t num_profile_a, num_profile_b;

void TFT_init(void);
void TFT_loop(uint16_t rpm, char curr_gear, char des_gear, char* text);

#endif /* TFT_H_ */
