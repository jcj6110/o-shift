/*
@file    tft.c
@brief   TFT handling functions for EVE_Test project, for FT81x only, probably works with BT81x
@version 1.10
@date    2019-01-04
@author  Rudolph Riedel

@section History

1.0
- initial release

1.1
- added a simple .png image that is drawn beneath the clock just as example of how it could be done

1.2
- replaced the clock with a scrolling line-strip
- moved the .png image over to the font-test section to make room
- added a scrolling bar display
- made scrolling depending on the state of the on/off button
- reduced the precision of VERTEX2F to 1 Pixel with VERTEXT_FORMAT() (FT81x only) to avoid some pointless "*16" multiplications

1.3
- adapted to release 3 of Ft8xx library with EVE_ and ft_ prefixes changed to EVE_
- removed "while (EVE_busy());" lines after EVE_cmd_execute() since it does that by itself now
- removed "EVE_cmd_execute();" line after EVE_cmd_loadimage(MEM_PIC1, EVE_OPT_NODL, pngpic, pngpic_size); as EVE_cmd_loadimage() executes itself now

1.4
- added num_profile_a and num_profile_b for simple profiling
- utilised EVE_cmd_start()
- some general cleanup and house-keeping to make it fit for release

1.5
- simplified the example layout a lot and made it to scale with all display-sizes

1.6
- a bit of house-keeping after trying a couple of things

1.7
- added a display for the amount of bytes generated in the display-list by the command co-pro

1.8
- moved the color settings from EVE_config.h to here

1.9
- changed FT8_ prefixes to EVE_

1.10
- some cleanup
 */

#include "EVE.h"
#include "EVE_config.h"
#include "EVE_commands.h"
#include "tft_data.h"

/* some pre-definded colors */
#define RED		0xff0000UL
#define ORANGE	0xffa500UL
#define GREEN	0x00ff00UL
#define BLUE	0x0000ffUL
#define BLUE_1	0x5dade2L
#define YELLOW	0xffff00UL
#define PINK	0xff00ffUL
#define PURPLE	0x800080UL
#define WHITE	0xffffffUL
#define BLACK	0x000000UL

#define MEM_DL_STATIC (EVE_RAM_G_SIZE - 4096) /* start-address of the static part of the display-list, upper 4k of gfx-mem */

uint32_t num_dl_static; /* amount of bytes in the static part of our display-list */
uint8_t tft_active = 0;

#define LAYOUT_Y1 66

void initStaticBackground()
{
	EVE_cmd_dl(CMD_DLSTART); /* Start the display list */
	EVE_cmd_dl(TAG(0)); /* do not use the following objects for touch-detection */
	EVE_cmd_bgcolor(0x000000); /* light grey */
	EVE_cmd_dl(VERTEX_FORMAT(0)); /* reduce precision for VERTEX2F to 1 pixel instead of 1/16 pixel default */
	EVE_cmd_dl(CLEAR_COLOR_RGB(207, 207, 207));
	EVE_cmd_dl(CLEAR(1,1,1));
	EVE_cmd_dl(DL_BEGIN | EVE_RECTS);
	EVE_cmd_dl(VERTEX2II(50, 450, 0, 0));
	EVE_cmd_dl(VERTEX2II(450, 250, 0, 0));
	EVE_cmd_dl(DL_END);
	EVE_cmd_dl(COLOR_RGB(0, 0, 0));
	EVE_cmd_text(250, 220, 31, EVE_OPT_CENTER, "MESSAGES");
	EVE_cmd_execute();
	num_dl_static = EVE_memRead16(REG_CMD_DL);
	EVE_cmd_memcpy(MEM_DL_STATIC, EVE_RAM_DL, num_dl_static);
	EVE_cmd_execute();
}


void TFT_init(void)
{
	if(EVE_init() != 0)
	{
		tft_active = 1;
		EVE_memWrite8(REG_PWM_DUTY, 0x30);	/* setup backlight, range is from 0 = off to 0x80 = max */
		initStaticBackground();
	}
}


/*
	dynamic portion of display-handling, meant to be called every 10ms or more
	divided into two sections:
		- handling of touch-events and variables
		- sending a new display-list to the FT8xx
*/
void TFT_loop(uint16_t rpm, char curr_gear, char des_gear, char* text)
{
	char buff[5] = {0};
	if(tft_active != 0)
	{
		EVE_start_cmd_burst(); /* start writing to the cmd-fifo as one stream of bytes, only sending the address once */

		EVE_cmd_dl(CMD_DLSTART); /* start the display list */
		EVE_cmd_dl(DL_CLEAR_RGB | WHITE); /* set the default clear color to white */
		EVE_cmd_dl(DL_CLEAR | CLR_COL | CLR_STN | CLR_TAG); /* clear the screen - this and the previous prevent artifacts between lists, Attributes are the color, stencil and tag buffers */
		EVE_cmd_dl(TAG(0));
		EVE_cmd_append(MEM_DL_STATIC, num_dl_static); /* insert static part of display-list from copy in gfx-mem */
		EVE_cmd_dl(TAG(1)); /* assign tag-value '1' to the button that follows */
		if (rpm < 6000) {
			EVE_cmd_bgcolor(BLACK); /* some grey */
		}
		else {
			EVE_cmd_bgcolor(0xff5e13);
		}
		EVE_cmd_dl(COLOR_RGB(0xcc, 0xcc, 0xcc));
		EVE_cmd_gauge(630, 240, 150, EVE_OPT_NOPOINTER, 8, 5, rpm, 8000);
		EVE_cmd_dl(COLOR_RGB(255,0,0));
		EVE_cmd_gauge(630, 240, 150, EVE_OPT_NOTICKS | EVE_OPT_NOBACK, 7, 5, rpm, 7000);
		EVE_cmd_dl(TAG(2)); /* no touch */
		EVE_cmd_bgcolor(RED);
		EVE_cmd_dl(COLOR_RGB(0,255,0));
		EVE_cmd_keys(50, 56, 400, 47, 31, EVE_OPT_FLAT | curr_gear, "RN123456");
		EVE_cmd_keys(50, 120, 400, 47, 31, EVE_OPT_FLAT | des_gear, "RN123456");
		EVE_cmd_dl(COLOR_RGB(0,0,0));
		EVE_cmd_text(60, 250, 31, 0, text);
		EVE_cmd_dl(DL_DISPLAY);	/* instruct the graphics processor to show the list */
		EVE_cmd_dl(CMD_SWAP); /* make this list active */

		EVE_end_cmd_burst(); /* stop writing to the cmd-fifo */
		EVE_cmd_start(); /* order the command co-processor to start processing its FIFO queue but do not wait for completion */
	}
}
