/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "can.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
QSPI_HandleTypeDef hqspi;

SD_HandleTypeDef hsd;

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* Private user code ---------------------------------------------------------*/

void lights(Can_Msg n)
{

  switch (n.payload[0]) {
    case 1:
      HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_7);
      break;

    case 2:
      //HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_7, GPIO_PIN_SET);
      HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_8);
      break;

    default:
      break;
  }



}



/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* MCU Configuration--------------------------------------------------------*/

  /* Dummy CAN message */
  CAN_TxHeaderTypeDef txHeader;

  CAN_RxHeaderTypeDef rxHeader;
  uint8_t rxPayload[8];

  HAL_StatusTypeDef ret;


  Can_Msg try1;

  try1.id = 0x01;
  try1.payload[0] = 0x1;
  try1.payload[1] = 0x2;
  try1.payload[2] = 0x2;
  try1.payload[3] = 0x1;
  try1.dlc = 4;

  Can_Msg try2;

  try2.id = 0x01;
  try2.payload[0] = 0x2;
  try2.payload[1] = 0x1;
  try2.payload[2] = 0x1;
  try2.payload[3] = 0x2;
  try2.dlc = 4;

  Can_Msg rec1;


  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  can_init();

  GPIO_InitTypeDef led_init;
  led_init.Pin = GPIO_PIN_7 | GPIO_PIN_8;
  led_init.Mode = GPIO_MODE_OUTPUT_PP;
  led_init.Pull = GPIO_NOPULL;
  led_init.Speed = GPIO_SPEED_FREQ_VERY_HIGH;

  HAL_GPIO_Init(GPIOB, &led_init);
  HAL_GPIO_WritePin(GPIOB, led_init.Pin, GPIO_PIN_RESET);

  // Sending some dummy bytes

  can2_transmit(&try1);
  //HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_SET);

  /* Infinite loop */
  while (1)
  {
      //HAL_GPIO_WritePin(GPIOB, (GPIO_PIN_7|GPIO_PIN_8), GPIO_PIN_RESET);
      //HAL_CAN_GetRxMessage(&hcan1, 0, &rxHeader, rxPayload);
      if(can_receive(&hcan1, &rec1) == 0){
        //memset(&rxHeader, 0, sizeof(CAN_RxHeaderTypeDef));
        //memset(rxPayload, 0, 8);
        //try1.id = 2;
        //HAL_CAN_AddTxMessage(&hcan1, &rxHeader, rxPayload, &mailbox);
        lights(rec1);
        for(uint32_t i = 0; i < 100000; i++);
        can1_transmit(&try1);
        for(uint32_t i = 0; i < 100000; i++);

        // Light the LED to indicate success
        //HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_7);
        //HAL_GPIO_WritePin(GPIOB, GPIO_PIN_7, GPIO_PIN_SET);
        if(can_receive(&hcan2, &rec1) == 0){
          //memset(&rxHeader, 0, sizeof(CAN_RxHeaderTypeDef));
          //memset(rxPayload, 0, 8);
          //try1.id = 2;
          //HAL_CAN_AddTxMessage(&hcan1, &rxHeader, rxPayload, &mailbox);
          //HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_8);
          lights(rec1);
          for(uint32_t i = 0; i < 100000; i++);
          can2_transmit(&try2);
          for(uint32_t i = 0; i < 100000; i++);
          //lights(rec1);
          // Light the LED to indicate success
          //HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET);

        }
      }
      //rxHeader.ExtId = 2;
      //rxHeader.StdId = 3;


    //}
/*
    if(hcan1.ErrorCode & HAL_CAN_ERROR_TIMEOUT)
    {
      while(1)
      {
        for(uint32_t i = 0; i < 100000; i++);
        HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_7);
      }
    }
    else if(hcan1.ErrorCode & HAL_CAN_ERROR_PARAM)
    {
      while(1)
      {
        for(uint32_t i = 0; i < 500000; i++);
        HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_7);
      }
    }

    // If we receive something, send it back. Standard IDs changed to 3
    /*if(HAL_CAN_GetRxFifoFillLevel(&hcan2, 0) > 0)
    {
      memset(&rxHeader, 0, sizeof(CAN_RxHeaderTypeDef));
      memset(rxPayload, 0, 8);

      HAL_CAN_GetRxMessage(&hcan2, 1, &rxHeader, rxPayload);

      rxHeader.ExtId = 2;
      rxHeader.StdId = 3;

      HAL_CAN_AddTxMessage(&hcan2, &rxHeader, rxPayload, &mailbox);

      // Light the LED to indicate success
      HAL_GPIO_WritePin(GPIOB, GPIO_PIN_8, GPIO_PIN_SET);
    }

    if(hcan2.ErrorCode & HAL_CAN_ERROR_TIMEOUT)
    {
      while(1)
      {
        for(uint32_t i = 0; i < 100000; i++);
        HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_8);
      }
    }
    else if(hcan2.ErrorCode & HAL_CAN_ERROR_PARAM)
    {
      while(1)
      {
        for(uint32_t i = 0; i < 500000; i++);
        HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_8);
      }
    }

    for(uint32_t i = 0; i < 500000; i++);
    */
  }
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 12;
  RCC_OscInitStruct.PLL.PLLN = 60;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 5;
  RCC_OscInitStruct.PLL.PLLR = 2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_SDIO|RCC_PERIPHCLK_CLK48;
  PeriphClkInitStruct.Clk48ClockSelection = RCC_CLK48CLKSOURCE_PLLQ;
  PeriphClkInitStruct.SdioClockSelection = RCC_SDIOCLKSOURCE_CLK48;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
