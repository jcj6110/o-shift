/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

#ifndef __shifter_dev_H
#define __shifter_dev_H

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f4xx_hal.h"
#include "gpio.h"
#include "usart.h"
#include "can.h"

typedef enum {
  REV,
  FIRST,
  SECOND,
  THIRD,
  FOURTH,
  FIFTH,
  SIXTH,
  NEUTRAL
} gear_t;

typedef enum {
  DISENGAGED,
  ENGAGED
} clutch_state_t;

uint8_t check_clutch(void);
gear_t check_gear(void);
void lockout_en(void);
void lockout_dis(void);
uint8_t comms_get_request(void);
void comms_send_packet(gear_t g_pos, clutch_state_t c_pos);

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
