/**
  ******************************************************************************
  * File Name          : gpio.c
  * Description        : This file provides code for the configuration
  *                      of all used GPIO pins.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

#include "shifter_dev.h"

uint8_t check_clutch(void) {
  uint8_t clutch_state;
  
  clutch_state = !sw_read(SW7);
  return clutch_state;
}

gear_t check_gear(void) {
  gear_t gear_pos = NEUTRAL;
  uint8_t sw_vals;
  
  sw_vals = sw_mask();
  sw_vals &= 0x7F;
  switch(sw_vals) {
    case 0x01: gear_pos = REV;     break;
    case 0x02: gear_pos = FIRST;   break;
    case 0x04: gear_pos = THIRD;   break;
    case 0x08: gear_pos = FIFTH;   break;
    case 0x10: gear_pos = SECOND;  break;
    case 0x20: gear_pos = FOURTH;  break;
    case 0x40: gear_pos = SIXTH;   break;
    default:   break;
  }
  return gear_pos;
}

void lockout_en(void) {
  servo_off();
  return;
}

void lockout_dis(void) {
  servo_on();
  return;
}

uint8_t comms_get_request(void) {
  Can_Msg pckt;
  if (can1_receive(&pckt)) {
    if (pckt.payload[0] == 0x55) {
      return 1;
    }
    else {
      return 0;
    }
  }
  return 0;
}

void comms_send_packet(gear_t g_pos, clutch_state_t c_pos) {
  Can_Msg tx;
  tx.id = 1;
  tx.payload[0] = g_pos;
  tx.payload[1] = c_pos;
  tx.dlc = 2;
  can1_transmit(&tx);
  return;
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
