/**
  ******************************************************************************
  * File Name          : USART.h
  * Description        : This file provides code for the configuration
  *                      of the USART instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __usart_H
#define __usart_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal_uart.h"
#include "stm32f4xx_hal_usart.h"
#include "stm32f4xx_hal.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart3;

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */


/* USER CODE BEGIN Prototypes */

/*
 * Initializes usart port
 *
 * returns: HAL status (0 success)
 */
int uart_init();

/*
 * Pushes char across UART
 *
 * returns: HAL status (0 success)
 */
int uart_putChar(char c, uint32_t timeout);

/*
 * Pushes string accross UART
 *
 * returns: HAL status (0 success)
 */
int uart_puts(char* buff, uint16_t size, uint32_t timeout);

/*
 * Gets character accross UART
 *
 * returns: HAL status (0 success)
 */
int uart_getChar(char *c,uint32_t timeout);

/*
 * Gets string accross UART
 *
 * returns: HAL status (0 success)
 */
 int uart_gets(char* buff, uint16_t size, uint32_t timeout);

/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif
#endif /*__ usart_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
