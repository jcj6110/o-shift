/**
  ******************************************************************************
  * File Name          : CAN.h
  * Description        : This file provides code for the configuration
  *                      of the CAN instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __can_H
#define __can_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_can.h"

/* USER CODE BEGIN Includes */
typedef struct
{
  uint32_t id;          // message ID
  uint8_t payload[8];   // message data bytes
  uint8_t dlc;          // number of bytes

}Can_Msg;

/* USER CODE END Includes */

extern CAN_HandleTypeDef hcan1;
extern CAN_HandleTypeDef hcan2;

/* USER CODE BEGIN Private defines */

void can_init(void);
void can1_transmit(Can_Msg *msg);
void can2_transmit(Can_Msg *msg);
int can1_receive(Can_Msg *msg);
int can2_receive(Can_Msg *msg);
int can_receive(CAN_HandleTypeDef *hcan, Can_Msg *msg);
/* USER CODE END Private defines */

void MX_CAN1_Init(void);
void MX_CAN2_Init(void);

void MX_CAN1_Start(void);
void MX_CAN2_Start(void);

void MX_CAN1_SetDefaultFilter(void);
void MX_CAN2_SetDefaultFilter(void);

/* USER CODE BEGIN Prototypes */

/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif
#endif /*__ can_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
