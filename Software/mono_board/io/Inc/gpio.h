/**
  ******************************************************************************
  * File Name          : gpio.h
  * Description        : This file contains all the functions prototypes for 
  *                      the gpio  
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

#ifndef __gpio_H
#define __gpio_H
#ifdef __cplusplus
 extern "C" {
#endif

#include "main.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"

#define SW0_pin  GPIO_PIN_0
#define SW0_port GPIOC
#define SW1_pin  GPIO_PIN_1
#define SW1_port GPIOC
#define SW2_pin  GPIO_PIN_2
#define SW2_port GPIOC
#define SW3_pin  GPIO_PIN_3
#define SW3_port GPIOC
#define SW4_pin  GPIO_PIN_0
#define SW4_port GPIOA
#define SW5_pin  GPIO_PIN_1
#define SW5_port GPIOA
#define SW6_pin  GPIO_PIN_2
#define SW6_port GPIOA
#define SW7_pin  GPIO_PIN_3
#define SW7_port GPIOA

#define SERVO1_EN_pin  GPIO_PIN_5
#define SERVO1_EN_port GPIOB
#define SERVO2_EN_pin  GPIO_PIN_6
#define SERVO2_EN_port GPIOB

#define LED0_pin  GPIO_PIN_7
#define LED0_port GPIOB
#define LED1_pin  GPIO_PIN_8
#define LED1_port GPIOB
#define LED2_pin  GPIO_PIN_9
#define LED2_port GPIOB

#define QSPI_NCS_pin GPIO_PIN_4
#define QSPI_NCS_port GPIOC

typedef enum {
  LED0,
  LED1,
  LED2
} LED;

typedef enum {
  SW0,
  SW1, 
  SW2,
  SW3,
  SW4,
  SW5,
  SW6,
  SW7
} SW;

void    GPIO_Init(void);
uint8_t sw_read(SW);
uint8_t sw_mask(void);
void    servo_toggle(void);
void    servo_on(void);
void    servo_off(void);
void    led_on(LED);
void    led_off(LED);
void    led_toggle(LED);
void    led_mask(uint8_t);
void    led_int_mask(uint8_t);
void    qspi_ncs_set_high(void);
void    qspi_ncs_set_low(void);

#ifdef __cplusplus
}
#endif
#endif

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
