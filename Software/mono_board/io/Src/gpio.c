/**
  ******************************************************************************
  * File Name          : gpio.c
  * Description        : This file provides code for the configuration
  *                      of all used GPIO pins.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

#include "gpio.h"

void GPIO_Init(void)
{
  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  GPIO_InitTypeDef SW0;
  GPIO_InitTypeDef SW1;
  GPIO_InitTypeDef SW2;
  GPIO_InitTypeDef SW3;
  GPIO_InitTypeDef SW4;
  GPIO_InitTypeDef SW5;
  GPIO_InitTypeDef SW6;
  GPIO_InitTypeDef SW7;
  GPIO_InitTypeDef SERVO1_EN;
  GPIO_InitTypeDef SERVO2_EN;
  GPIO_InitTypeDef LED0;
  GPIO_InitTypeDef LED1;
  GPIO_InitTypeDef LED2;
  GPIO_InitTypeDef QSPI_NCS;

  SW0.Pin = SW0_pin;
  SW0.Mode = GPIO_MODE_INPUT;
  SW0.Pull = GPIO_NOPULL;
  SW0.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  SW1.Pin = SW1_pin;
  SW1.Mode = GPIO_MODE_INPUT;
  SW1.Pull = GPIO_NOPULL;
  SW1.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  SW2.Pin = SW2_pin;
  SW2.Mode = GPIO_MODE_INPUT;
  SW2.Pull = GPIO_NOPULL;
  SW2.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  SW3.Pin = SW3_pin;
  SW3.Mode = GPIO_MODE_INPUT;
  SW3.Pull = GPIO_NOPULL;
  SW3.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  SW4.Pin = SW4_pin;
  SW4.Mode = GPIO_MODE_INPUT;
  SW4.Pull = GPIO_NOPULL;
  SW4.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  SW5.Pin = SW5_pin;
  SW5.Mode = GPIO_MODE_INPUT;
  SW5.Pull = GPIO_NOPULL;
  SW5.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  SW6.Pin = SW6_pin;
  SW6.Mode = GPIO_MODE_INPUT;
  SW6.Pull = GPIO_NOPULL;
  SW6.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  SW7.Pin = SW7_pin;
  SW7.Mode = GPIO_MODE_INPUT;
  SW7.Pull = GPIO_PULLUP;
  SW7.Speed = GPIO_SPEED_FREQ_VERY_HIGH;

  SERVO1_EN.Pin = SERVO1_EN_pin;
  SERVO1_EN.Mode = GPIO_MODE_OUTPUT_PP;
  SERVO1_EN.Pull = GPIO_NOPULL;
  SERVO1_EN.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  SERVO2_EN.Pin = SERVO2_EN_pin;
  SERVO2_EN.Mode = GPIO_MODE_OUTPUT_PP;
  SERVO2_EN.Pull = GPIO_NOPULL;
  SERVO2_EN.Speed = GPIO_SPEED_FREQ_VERY_HIGH;

  LED0.Pin = LED0_pin;
  LED0.Mode = GPIO_MODE_OUTPUT_PP;
  LED0.Pull = GPIO_NOPULL;
  LED0.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  LED1.Pin = LED1_pin;
  LED1.Mode = GPIO_MODE_OUTPUT_PP;
  LED1.Pull = GPIO_NOPULL;
  LED1.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  LED2.Pin = LED2_pin;
  LED2.Mode = GPIO_MODE_OUTPUT_PP;
  LED2.Pull = GPIO_NOPULL;
  LED2.Speed = GPIO_SPEED_FREQ_VERY_HIGH;

  QSPI_NCS.Pin = QSPI_NCS_pin;
  QSPI_NCS.Mode = GPIO_MODE_OUTPUT_PP;
  QSPI_NCS.Pull = GPIO_NOPULL;
  QSPI_NCS.Speed = GPIO_SPEED_FREQ_VERY_HIGH;

  HAL_GPIO_Init(SW0_port, &SW0);
  HAL_GPIO_Init(SW1_port, &SW1);
  HAL_GPIO_Init(SW2_port, &SW2);
  HAL_GPIO_Init(SW3_port, &SW3);
  HAL_GPIO_Init(SW4_port, &SW4);
  HAL_GPIO_Init(SW5_port, &SW5);
  HAL_GPIO_Init(SW6_port, &SW6);
  HAL_GPIO_Init(SW7_port, &SW7);

  HAL_GPIO_Init(SERVO1_EN_port, &SERVO1_EN);
  HAL_GPIO_Init(SERVO2_EN_port, &SERVO2_EN);

  HAL_GPIO_Init(LED0_port, &LED0);
  HAL_GPIO_Init(LED1_port, &LED1);
  HAL_GPIO_Init(LED2_port, &LED2);

  HAL_GPIO_Init(QSPI_NCS_port, &QSPI_NCS);

  HAL_GPIO_WritePin(SERVO1_EN_port, SERVO1_EN_pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(SERVO2_EN_port, SERVO2_EN_pin, GPIO_PIN_RESET);

  HAL_GPIO_WritePin(LED0_port, LED0_pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(LED1_port, LED1_pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(LED2_port, LED2_pin, GPIO_PIN_RESET);

  HAL_GPIO_WritePin(QSPI_NCS_port, QSPI_NCS_pin, GPIO_PIN_SET);

  return;
}

void qspi_ncs_set_high(void) {
  HAL_GPIO_WritePin(QSPI_NCS_port, QSPI_NCS_pin, GPIO_PIN_SET);
  return;
}

void qspi_ncs_set_low(void) {
  HAL_GPIO_WritePin(QSPI_NCS_port, QSPI_NCS_pin, GPIO_PIN_RESET);
  return;
}

uint8_t sw_read(SW  sw_num) {
  GPIO_PinState sw_val = GPIO_PIN_RESET;
  switch(sw_num) {
    case SW0: sw_val = HAL_GPIO_ReadPin(SW0_port, SW0_pin); break;
    case SW1: sw_val = HAL_GPIO_ReadPin(SW1_port, SW1_pin); break;
    case SW2: sw_val = HAL_GPIO_ReadPin(SW2_port, SW2_pin); break;
    case SW3: sw_val = HAL_GPIO_ReadPin(SW3_port, SW3_pin); break;
    case SW4: sw_val = HAL_GPIO_ReadPin(SW4_port, SW4_pin); break;
    case SW5: sw_val = HAL_GPIO_ReadPin(SW5_port, SW5_pin); break;
    case SW6: sw_val = HAL_GPIO_ReadPin(SW6_port, SW6_pin); break;
    case SW7: sw_val = HAL_GPIO_ReadPin(SW7_port, SW7_pin); break;
    default: break;
  }
  return sw_val;
}

uint8_t sw_mask(void) {
  uint8_t mask = 0;
  uint8_t temp;

  temp = !sw_read(SW7);
  mask += temp * 0x80;
  temp = !sw_read(SW6);
  mask += temp * 0x40;
  temp = !sw_read(SW5);
  mask += temp * 0x20;
  temp = !sw_read(SW4);
  mask += temp * 0x10;
  temp = !sw_read(SW3);
  mask += temp * 0x08;
  temp = !sw_read(SW2);
  mask += temp * 0x04;
  temp = !sw_read(SW1);
  mask += temp * 0x02;
  temp = !sw_read(SW0);
  mask += temp * 0x01;

  return mask;
}

void servo_toggle(void) {
  HAL_GPIO_TogglePin(SERVO1_EN_port, SERVO1_EN_pin);
  HAL_GPIO_TogglePin(SERVO2_EN_port, SERVO2_EN_pin);
  return;
}

void servo_on(void) {
  HAL_GPIO_WritePin(SERVO1_EN_port, SERVO1_EN_pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(SERVO2_EN_port, SERVO2_EN_pin, GPIO_PIN_SET);
  return;
}

void servo_off(void) {
  HAL_GPIO_WritePin(SERVO1_EN_port, SERVO1_EN_pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(SERVO2_EN_port, SERVO2_EN_pin, GPIO_PIN_RESET);
  return;
}

void led_on(LED led_num) {
  switch(led_num) {
    case LED0: HAL_GPIO_WritePin(LED0_port, LED0_pin, GPIO_PIN_SET); break;
    case LED1: HAL_GPIO_WritePin(LED1_port, LED1_pin, GPIO_PIN_SET); break;
    case LED2: HAL_GPIO_WritePin(LED2_port, LED2_pin, GPIO_PIN_SET); break;
    default: break;
  }
  return;
}

void led_off(LED led_num) {
  switch(led_num) {
    case LED0: HAL_GPIO_WritePin(LED0_port, LED0_pin, GPIO_PIN_RESET); break;
    case LED1: HAL_GPIO_WritePin(LED1_port, LED1_pin, GPIO_PIN_RESET); break;
    case LED2: HAL_GPIO_WritePin(LED2_port, LED2_pin, GPIO_PIN_RESET); break;
    default: break;
  }
  return;
}

void led_toggle(LED led_num) {
  switch(led_num) {
    case LED0: HAL_GPIO_TogglePin(LED0_port, LED0_pin); break;
    case LED1: HAL_GPIO_TogglePin(LED1_port, LED1_pin); break;
    case LED2: HAL_GPIO_TogglePin(LED2_port, LED2_pin); break;
    default: break;
  }
  return;
}

void led_mask(uint8_t mask) {
  led_off(LED0);
  led_off(LED1);
  led_off(LED2);
  if (mask >= 0x80) {
    mask -= 0x80;
  }
  switch(mask) {
    case 0x00: break;
    case 0x01: led_on(LED0); break;
    case 0x02: led_on(LED1); break;
    case 0x04: led_on(LED0); led_on(LED1); break;
    case 0x08: led_on(LED2); break;
    case 0x10: led_on(LED2); led_on(LED0); break;
    case 0x20: led_on(LED2); led_on(LED1); break;
    case 0x40: led_on(LED2); led_on(LED1); led_on(LED0); break;
    default: break;
  }
  return;
}

void led_int_mask(uint8_t mask) {
  led_off(LED0);
  led_off(LED1);
  led_off(LED2);
  if (mask >= 0x80) {
    mask -= 0x80;
  }
  switch(mask) {
    case 0x00: break;
    case 0x01: led_on(LED0); break;
    case 0x02: led_on(LED1); break;
    case 0x03: led_on(LED0); led_on(LED1); break;
    case 0x04: led_on(LED2); break;
    case 0x05: led_on(LED2); led_on(LED0); break;
    case 0x06: led_on(LED2); led_on(LED1); break;
    case 0x07: led_on(LED2); led_on(LED1); led_on(LED0); break;
    default: break;
  }
  return;
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
