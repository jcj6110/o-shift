/**
  ******************************************************************************
  * File Name          : CAN.c
  * Description        : This file provides code for the configuration
  *                      of the CAN instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "can.h"

//Added includes
#include <string.h>
//


CAN_HandleTypeDef hcan1;
CAN_HandleTypeDef hcan2;

//Added definitions
GPIO_InitTypeDef Initgpio1;
GPIO_InitTypeDef Initgpio2;
CAN_FilterTypeDef filterdf;

/*
 _____________________
|   CAN 1  |  CAN 2   |
|----------|----------|
|PA11 : Rx |PB12 : Rx |
|PA12 : Tx |PB13 : Tx |
 ---------------------
*/



//Begin of Added code
void can_init(void)
{

  //Initializes the clock for CAN1 & CAN2 if they are disabled
    if(!(__HAL_RCC_CAN1_IS_CLK_ENABLED()))
        __HAL_RCC_CAN1_CLK_ENABLE();
    if(!(__HAL_RCC_CAN2_IS_CLK_ENABLED()))
        __HAL_RCC_CAN2_CLK_ENABLE();

    //GPIO for CAN1 configuration
    Initgpio1.Pin = GPIO_PIN_11 | GPIO_PIN_12;//Add the correct pin for CAN1
    Initgpio1.Mode = GPIO_MODE_AF_PP;
    Initgpio1.Pull = GPIO_PULLUP;
    Initgpio1.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    Initgpio1.Alternate = GPIO_AF9_CAN1;

    //GPIO for CAN2 configuration
    Initgpio2.Pin = GPIO_PIN_12 | GPIO_PIN_13;//Add the correct pin for CAN2
    Initgpio2.Mode = GPIO_MODE_AF_OD;
    Initgpio2.Pull = GPIO_PULLUP;
    Initgpio2.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    Initgpio2.Alternate = GPIO_AF9_CAN2;


    __HAL_RCC_GPIOB_CLK_ENABLE(); //Change x for the right port bank
    HAL_GPIO_Init(GPIOB, &Initgpio2); //Change x for the right port bank

    __HAL_RCC_GPIOA_CLK_ENABLE(); //Change x for the right port bank
    HAL_GPIO_Init(GPIOA, &Initgpio1); //Change x for the right port bank

    //Initialize all peripherals
    MX_CAN1_Init();
    MX_CAN2_Init();

    //Filter configuration
    MX_CAN1_SetDefaultFilter();
    MX_CAN2_SetDefaultFilter();

    MX_CAN1_Start();
    MX_CAN2_Start();

}


void can1_transmit(Can_Msg *msg)
{

    uint32_t mailbox = CAN_TX_MAILBOX0;
    CAN_TxHeaderTypeDef txheader;

    txheader.ExtId = msg->id;
    txheader.IDE = CAN_ID_EXT;
    txheader.RTR = CAN_RTR_DATA;
    txheader.DLC = msg->dlc;
    txheader.TransmitGlobalTime = DISABLE;

    HAL_CAN_AddTxMessage(&hcan1, &txheader, msg->payload, &mailbox);

}

void can2_transmit(Can_Msg *msg)
{

    uint32_t mailbox = CAN_TX_MAILBOX0;
    CAN_TxHeaderTypeDef txheader;

    txheader.StdId = msg->id;
    txheader.IDE = CAN_ID_STD;
    txheader.RTR = CAN_RTR_DATA;
    txheader.DLC = msg->dlc;
    txheader.TransmitGlobalTime = DISABLE;

    HAL_CAN_AddTxMessage(&hcan2, &txheader, msg->payload, &mailbox);

}

int can1_receive(Can_Msg *msg)
{
    uint8_t data[8];
    CAN_RxHeaderTypeDef rxheader;

    if(HAL_CAN_GetRxFifoFillLevel(&hcan1,0) > 0)
    {
        HAL_CAN_GetRxMessage(&hcan1,0 ,&rxheader, data);
        msg->id = rxheader.ExtId;
        msg->dlc = rxheader.DLC;
        memcpy(msg->payload,data,8);
        return 1; //Message
    }
    else
    {
        return 0; //No message
    }
}

int can2_receive(Can_Msg *msg)
{
    uint8_t data[8];
    CAN_RxHeaderTypeDef rxheader;

    if(HAL_CAN_GetRxFifoFillLevel(&hcan2,0) > 0)
    {
        HAL_CAN_GetRxMessage(&hcan2,0 ,&rxheader, data);
        msg->id = rxheader.StdId;
        msg->dlc = rxheader.DLC;
        memcpy(msg->payload,data,8);
        return 1; //Message
    }
    else
    {
        return 0; //No message
    }
}

/* CAN1 init function */
void MX_CAN1_Init(void)
{

  hcan1.Instance = CAN1;
  hcan1.Init.Prescaler = 4;
  hcan1.Init.Mode = CAN_MODE_NORMAL;
  hcan1.Init.SyncJumpWidth = CAN_SJW_2TQ;
  hcan1.Init.TimeSeg1 = CAN_BS1_3TQ;
  hcan1.Init.TimeSeg2 = CAN_BS2_4TQ;
  hcan1.Init.TimeTriggeredMode = DISABLE;
  hcan1.Init.AutoBusOff = DISABLE;
  hcan1.Init.AutoWakeUp = DISABLE;
  hcan1.Init.AutoRetransmission = DISABLE;
  hcan1.Init.ReceiveFifoLocked = DISABLE;
  hcan1.Init.TransmitFifoPriority = DISABLE;
  if (HAL_CAN_Init(&hcan1) != HAL_OK)
  {
    Error_Handler();
  }

}
/* CAN2 init function */
void MX_CAN2_Init(void)
{

  hcan2.Instance = CAN2;
  hcan2.Init.Prescaler = 4;
  hcan2.Init.Mode = CAN_MODE_NORMAL;
  hcan2.Init.SyncJumpWidth = CAN_SJW_2TQ;
  hcan2.Init.TimeSeg1 = CAN_BS1_3TQ;
  hcan2.Init.TimeSeg2 = CAN_BS2_4TQ;
  hcan2.Init.TimeTriggeredMode = DISABLE;
  hcan2.Init.AutoBusOff = DISABLE;
  hcan2.Init.AutoWakeUp = DISABLE;
  hcan2.Init.AutoRetransmission = DISABLE;
  hcan2.Init.ReceiveFifoLocked = DISABLE;
  hcan2.Init.TransmitFifoPriority = DISABLE;
  if (HAL_CAN_Init(&hcan2) != HAL_OK)
  {
    Error_Handler();
  }

}

static uint32_t HAL_RCC_CAN_CLK_ENABLED=0;

void HAL_CAN_MspInit(CAN_HandleTypeDef* canHandle)
{
    return;
}

void HAL_CAN_MspDeInit(CAN_HandleTypeDef* canHandle)
{

  if(canHandle->Instance==CAN1)
  {
    /* Peripheral clock disable */
    HAL_RCC_CAN_CLK_ENABLED--;
    if(HAL_RCC_CAN_CLK_ENABLED==0){
      __HAL_RCC_CAN1_CLK_DISABLE();
    }

    /**CAN1 GPIO Configuration
    PA11     ------> CAN1_RX
    PA12     ------> CAN1_TX
    */
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_11|GPIO_PIN_12);

  }
  else if(canHandle->Instance==CAN2)
  {
    /* Peripheral clock disable */
    __HAL_RCC_CAN2_CLK_DISABLE();
    HAL_RCC_CAN_CLK_ENABLED--;
    if(HAL_RCC_CAN_CLK_ENABLED==0){
      __HAL_RCC_CAN1_CLK_DISABLE();
    }

    /**CAN2 GPIO Configuration
    PB12     ------> CAN2_RX
    PB13     ------> CAN2_TX
    */
    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_12|GPIO_PIN_13);
  }
}

void MX_CAN1_Start()
{
  HAL_CAN_Start(&hcan1);
}

void MX_CAN2_Start()
{
  HAL_CAN_Start(&hcan2);
}

void MX_CAN1_SetDefaultFilter()
{
  CAN_FilterTypeDef filter;

  filter.FilterMode = CAN_FILTERMODE_IDMASK; // Filter will apply a mask to determine which messages should be received
  filter.FilterScale = CAN_FILTERSCALE_32BIT;

  filter.FilterIdHigh = 0;
  filter.FilterIdLow = 0;
  filter.FilterMaskIdHigh = 0;
  filter.FilterMaskIdLow = 0;
  filter.FilterFIFOAssignment = CAN_FILTER_FIFO0;
  filter.FilterBank = 0;

  filter.FilterActivation = CAN_FILTER_ENABLE;

  HAL_CAN_ConfigFilter(&hcan1, &filter);
}

void MX_CAN2_SetDefaultFilter()
{
  CAN_FilterTypeDef filter;

  filter.FilterMode = CAN_FILTERMODE_IDMASK; // Filter will apply a mask to determine which messages should be received
  filter.FilterScale = CAN_FILTERSCALE_32BIT;

  filter.FilterIdHigh = 0x7E8 << 5; // PID Response ID
  filter.FilterIdLow = 0;
  filter.FilterMaskIdHigh = 0xFFFF; // All bits must match
  filter.FilterMaskIdLow = 0;
  filter.FilterFIFOAssignment = CAN_FILTER_FIFO0;
  filter.FilterBank = 14; // Slaves start at the upper half of the filter banks (14)
  filter.SlaveStartFilterBank = 14;

  filter.FilterActivation = CAN_FILTER_ENABLE;

  HAL_CAN_ConfigFilter(&hcan2, &filter);
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
