/**
  ******************************************************************************
  * File Name          : QUADSPI.c
  * Description        : This file provides code for the configuration
  *                      of the QUADSPI instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "quadspi.h"
#include "stm32f4xx_hal_qspi.h"
#include "stm32f4xx_hal_rcc_ex.h"


QSPI_HandleTypeDef hqspi;
static uint32_t numberOfDataLines = QSPI_DATA_1_LINE;
static uint32_t numberOfAddressLines = QSPI_ADDRESS_1_LINE;


/* QUADSPI init function */
void MX_QUADSPI_Init(void)
{

  hqspi.Instance = QUADSPI;
  hqspi.Init.ClockPrescaler = 5;
  hqspi.Init.FifoThreshold = 10;
  hqspi.Init.SampleShifting = QSPI_SAMPLE_SHIFTING_NONE;
  hqspi.Init.FlashSize = 31;
  hqspi.Init.ChipSelectHighTime = QSPI_CS_HIGH_TIME_1_CYCLE;
  hqspi.Init.ClockMode = QSPI_CLOCK_MODE_0;
  hqspi.Init.FlashID = QSPI_FLASH_ID_1;
  hqspi.Init.DualFlash = QSPI_DUALFLASH_DISABLE;
  if (HAL_QSPI_Init(&hqspi) != HAL_OK)
  {
    Error_Handler();
  }

}

void HAL_QSPI_MspInit(QSPI_HandleTypeDef* qspiHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(qspiHandle->Instance==QUADSPI)
  {
    /* QUADSPI clock enable */
    __HAL_RCC_QSPI_CLK_ENABLE();
  

    /* Reset QUADSPI IP */
    __HAL_RCC_QSPI_FORCE_RESET();
    __HAL_RCC_QSPI_RELEASE_RESET();

    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
    /**QUADSPI GPIO Configuration    
    PB2     ------> QUADSPI_CLK
    PC9     ------> QUADSPI_BK1_IO0
    PC10     ------> QUADSPI_BK1_IO1
    PB6     ------> QUADSPI_BK1_NCS 
    */
    GPIO_InitStruct.Pin = GPIO_PIN_2;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF9_QSPI;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_9|GPIO_PIN_10;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF9_QSPI;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_6;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF10_QSPI;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
  }
}

void HAL_QSPI_MspDeInit(QSPI_HandleTypeDef* qspiHandle)
{

  if(qspiHandle->Instance==QUADSPI)
  {
    /* Peripheral clock disable */
    __HAL_RCC_QSPI_CLK_DISABLE();
  
    /**QUADSPI GPIO Configuration    
    PB2     ------> QUADSPI_CLK
    PC9     ------> QUADSPI_BK1_IO0
    PC10     ------> QUADSPI_BK1_IO1
    PB6     ------> QUADSPI_BK1_NCS 
    */
    HAL_GPIO_DeInit(GPIOB, GPIO_PIN_2|GPIO_PIN_6);

    HAL_GPIO_DeInit(GPIOC, GPIO_PIN_9|GPIO_PIN_10);

  }
}

void qspiTransmit(uint32_t address, uint32_t numDummyCycles, uint8_t * data, uint16_t length)
{
    QSPI_CommandTypeDef command;
    command.Instruction         = 0;
    command.InstructionMode     = QSPI_INSTRUCTION_NONE;
    command.Address             = address;
    command.AddressMode         = numberOfAddressLines;
    command.AddressSize         = QSPI_ADDRESS_24_BITS;
    command.AlternateBytes      = 0;
    command.AlternateByteMode   = QSPI_ALTERNATE_BYTES_NONE;
    command.AlternateBytesSize  = QSPI_ALTERNATE_BYTES_8_BITS;
    command.DummyCycles         = numDummyCycles;
    command.DataMode            = numberOfDataLines;
    command.NbData              = length;
    command.DdrMode             = QSPI_DDR_MODE_DISABLE;
    command.DdrHoldHalfCycle    = QSPI_DDR_HHC_ANALOG_DELAY;
    command.SIOOMode            = QSPI_SIOO_INST_EVERY_CMD;

    HAL_QSPI_Command(&hqspi, &command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE);

    HAL_QSPI_Transmit(&hqspi, data, HAL_QPSI_TIMEOUT_DEFAULT_VALUE);
}

void qspiTransmitRaw(uint8_t * data, uint16_t length)
{
    QSPI_CommandTypeDef command;
    command.Instruction         = 0;
    command.InstructionMode     = QSPI_INSTRUCTION_NONE;
    command.Address             = 0;
    command.AddressMode         = QSPI_ADDRESS_NONE;
    command.AddressSize         = QSPI_ADDRESS_24_BITS;
    command.AlternateBytes      = 0;
    command.AlternateByteMode   = QSPI_ALTERNATE_BYTES_NONE;
    command.AlternateBytesSize  = QSPI_ALTERNATE_BYTES_8_BITS;
    command.DummyCycles         = 0;
    command.DataMode            = numberOfDataLines;
    command.NbData              = length;
    command.DdrMode             = QSPI_DDR_MODE_DISABLE;
    command.DdrHoldHalfCycle    = QSPI_DDR_HHC_ANALOG_DELAY;
    command.SIOOMode            = QSPI_SIOO_INST_EVERY_CMD;

  HAL_QSPI_Command(&hqspi, &command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE);
  HAL_QSPI_Transmit(&hqspi, data, HAL_QPSI_TIMEOUT_DEFAULT_VALUE);
}

void qspiReceive(uint32_t address, uint32_t numDummyCycles, uint8_t * data, uint16_t length)
{
    QSPI_CommandTypeDef command;
    command.Instruction         = 0;
    command.InstructionMode     = QSPI_INSTRUCTION_NONE;
    command.Address             = address;
    command.AddressMode         = numberOfAddressLines;
    command.AddressSize         = QSPI_ADDRESS_24_BITS;
    command.AlternateBytes      = 0;
    command.AlternateByteMode   = QSPI_ALTERNATE_BYTES_NONE;
    command.AlternateBytesSize  = QSPI_ALTERNATE_BYTES_8_BITS;
    command.DummyCycles         = numDummyCycles;
    command.DataMode            = numberOfDataLines;
    command.NbData              = length;
    command.DdrMode             = QSPI_DDR_MODE_DISABLE;
    command.DdrHoldHalfCycle    = QSPI_DDR_HHC_ANALOG_DELAY;
    command.SIOOMode            = QSPI_SIOO_INST_EVERY_CMD;

    HAL_QSPI_Command(&hqspi, &command, HAL_QPSI_TIMEOUT_DEFAULT_VALUE);

    HAL_QSPI_Receive(&hqspi, data, HAL_QPSI_TIMEOUT_DEFAULT_VALUE);
}


void qspiSetNumberOfLines(uint8_t numLines)
{
    switch(numLines)
    {
        case 0:
            numberOfDataLines = QSPI_DATA_NONE;
            numberOfAddressLines = QSPI_ADDRESS_NONE;
            break;
        case 1:
            numberOfDataLines = QSPI_DATA_1_LINE;
            numberOfAddressLines = QSPI_ADDRESS_1_LINE;
            break;
        case 2:
            numberOfDataLines = QSPI_DATA_2_LINES;
            numberOfAddressLines = QSPI_ADDRESS_2_LINES;
            break;
        case 4:
            numberOfDataLines = QSPI_DATA_4_LINES;
            numberOfAddressLines = QSPI_ADDRESS_4_LINES;
            break;
        default:
            numberOfDataLines = QSPI_DATA_NONE;
            numberOfAddressLines = QSPI_ADDRESS_NONE;
            break;
    }
}


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
